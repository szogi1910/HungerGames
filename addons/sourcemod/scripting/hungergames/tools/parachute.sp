bool g_bParachute[MAXPLAYERS+1];
Handle g_hGiveParachuteTimer[MAXPLAYERS+1] = {null, ...};

bool g_bParachuteEnabled[MAXPLAYERS+1];
int g_iParachuteEntityRef[MAXPLAYERS+1] = {INVALID_ENT_REFERENCE, ...};

void GiveParachuteDelayed(int client)
{
	ResetGiveParachuteTimer(client);
	
	if (g_fGiveParachuteDelay >= 0.0)
		g_hGiveParachuteTimer[client] = CreateTimer(g_fGiveParachuteDelay, Timer_GiveParachute, client);
}

void ResetGiveParachuteTimer(int client)
{
	if(g_hGiveParachuteTimer[client] != null)
	{
		CloseHandle(g_hGiveParachuteTimer[client]);
		g_hGiveParachuteTimer[client] = null;
	}
}

public Action Timer_GiveParachute(Handle timer, any client)
{
	GiveParachute(client, true);
	g_hGiveParachuteTimer[client] = null;
	
	DeleteParachuteLoot();
	
	return Plugin_Handled;
}

void GiveParachute(int client, bool msg = false)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsTribute[client])
		return;
	
	if(HasParachute(client))
		return;
		
	g_bParachute[client] = true;
	
	if(msg)
		CPrintToChat(client, "%T", "GotParachute", client, g_sPrefix);
}

bool HasParachute(int client)
{
	return g_bParachute[client];
}

bool HasParachuteEnabled(int client)
{
	return g_bParachuteEnabled[client];
}

void ResetParachute(int client)
{
	DisableParachute(client);
	g_bParachute[client] = false;
}

void DisableParachute(int client)
{
	int iEntity = EntRefToEntIndex(g_iParachuteEntityRef[client]);
	if(iEntity != INVALID_ENT_REFERENCE)
	{
		AcceptEntityInput(iEntity, "ClearParent");
		AcceptEntityInput(iEntity, "kill");
	}
	
	g_bParachuteEnabled[client] = false;
	g_iParachuteEntityRef[client] = INVALID_ENT_REFERENCE;
}

void AbortParachute(int client)
{
	DisableParachute(client);
}

void CheckParachute(int client, int buttons, int weapon)
{
	// Check abort reasons
	if(HasParachuteEnabled(client))
	{
		// Abort by released button
		if(!(buttons & IN_USE) || !IsPlayerAlive(client))
		{
			AbortParachute(client);
			return;
		}
		
		// Abort by up speed
		float fVel[3];
		GetEntDataVector(client, g_iVelocity, fVel);
		
		if(fVel[2] >= 0.0)
		{
			AbortParachute(client);
			return;
		}
		
		// Abort by on ground flag
		if(GetEntityFlags(client) & FL_ONGROUND)
		{
			AbortParachute(client);
			BlockWeapon(client, weapon, 1.0);
			return;
		}
		
		// decrease fallspeed
		float fOldSpeed = fVel[2];
		
		// Player is falling to fast, lets slow him to max fallspeed
		if(fVel[2] < fParachuteFallspeedMax*(-1.0))
			fVel[2] = fParachuteFallspeedMax * (-1.0);
		
		// Fallspeed changed
		if(fOldSpeed != fVel[2])
			TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVel);
		
		if(!bParachuteAllowShooting)
			BlockWeapon(client, weapon, 1.0);
	}
	// Should we start the parashute?
	else if(HasParachute(client))
	{
		// Reject by released button
		if(!(buttons & IN_USE))
			return;
		
		// Reject by on ground flag
		if(GetEntityFlags(client) & FL_ONGROUND)
			return;
		
		// Reject by up speed
		float fVel[3];
		GetEntDataVector(client, g_iVelocity, fVel);
		
		if(fVel[2] >= 0.0)
			return;
		
		// Open parachute
		int iEntity = CreateEntityByName("prop_dynamic_override");
		DispatchKeyValue(iEntity, "model", g_sParachuteModel);
		DispatchSpawn(iEntity);
		
		SetEntityMoveType(iEntity, MOVETYPE_NOCLIP);
		
		// Teleport to player
		float fPos[3];
		float fAng[3];
		GetClientAbsOrigin(client, fPos);
		GetClientAbsAngles(client, fAng);
		fAng[0] = 0.0;
		TeleportEntity(iEntity, fPos, fAng, NULL_VECTOR);
		
		// Parent to player
		char sClient[16];
		Format(sClient, 16, "client%d", client);
		DispatchKeyValue(client, "targetname", sClient);
		SetVariantString(sClient);
		AcceptEntityInput(iEntity, "SetParent", iEntity, iEntity, 0);
		
		g_iParachuteEntityRef[client] = EntIndexToEntRef(iEntity);
		g_bParachuteEnabled[client] = true;
	}
}