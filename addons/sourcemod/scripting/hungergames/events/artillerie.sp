/* Settings */

float g_fArtillerieHeightMin = 256.0;
float g_fArtillerieHeightMax = 2048.0;
float g_fArtillerieHeightDefault = 512.0;

int g_iArtilleriesLeft;
float g_iArtilleriePos[3];

int g_iArtilleryTarget;

void StartArtillerieAirStrike(int target)
{
	g_iArtilleryTarget = target;
	
	g_iArtilleriesLeft = 0;
	
	float distance;
	GetRoofDistance(g_iArtilleryTarget, distance);
	
	if(distance > g_fArtillerieHeightMin)
	{
		GetClientAbsOrigin(g_iArtilleryTarget, g_iArtilleriePos);
		
		if(distance > g_fArtillerieHeightMax)
			distance = g_fArtillerieHeightMax;
	}
	else distance = g_fArtillerieHeightDefault;
			
	g_iArtilleriePos[2] += distance;
	
	g_iArtilleriesLeft = g_iEventArtillerieProjectiles;
	
	CreateTimer(0.0, Timer_Artillerie, _, TIMER_FLAG_NO_MAPCHANGE);
}

public Action Timer_Artillerie(Handle timer, any data)
{
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
	
	g_iArtilleriesLeft--;
	
	CreateArtillerie();
	
	if(g_iArtilleriesLeft > 0)
		CreateTimer(0.3, Timer_Artillerie, _, TIMER_FLAG_NO_MAPCHANGE);
	
	return Plugin_Stop;
}

void CreateArtillerie()
{
	int iEntity = CreateEntityByName("hegrenade_projectile");
	if(iEntity == INVALID_ENT_REFERENCE)
		return;
		
	float pos[3];
	pos[0] = g_iArtilleriePos[0] + GetRandomFloat(-g_fEventArtillerieRadius, g_fEventArtillerieRadius);
	pos[1] = g_iArtilleriePos[1] + GetRandomFloat(-g_fEventArtillerieRadius, g_fEventArtillerieRadius);
	pos[2] = g_iArtilleriePos[2];
	
	SetEntPropEnt(iEntity, Prop_Send, "m_hThrower", 0);
	SetEntPropEnt(iEntity, Prop_Send, "m_hOwnerEntity", 0);
	SetEntProp(iEntity, Prop_Send, "m_iTeamNum", 2);

	TeleportEntity(iEntity, pos, NULL_VECTOR, NULL_VECTOR);
	DispatchSpawn(iEntity);
}

public void OnHegrenadeProjectile(int iEntity)
{
	SDKUnhook(iEntity, SDKHook_Spawn, OnHegrenadeProjectile);
	
	int client = GetEntPropEnt(iEntity, Prop_Send, "m_hOwnerEntity");
	// Ignore entitys owned by a player
	if(client != 0)
		return;
		
	SDKHook(iEntity, SDKHook_StartTouch, StartTouchHegrenade);
	
	int iRef = EntIndexToEntRef(iEntity);
	CreateTimer(0.0, Timer_DefuseHegrenade, iRef);
	CreateTimer(0.0, Timer_ColorHeGrenade, iRef);
}

public Action StartTouchHegrenade(int iEntity, int iEntity2)
{
	SDKUnhook(iEntity, SDKHook_StartTouch, StartTouchHegrenade);
	
	int iRef = EntIndexToEntRef(iEntity);
	CreateTimer(0.0, Timer_DetonateHegrenade, iRef);
}

public Action Timer_ColorHeGrenade(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
	{
		TE_SetupBeamFollow(ent, g_iLaser, 0, 1.0, 3.0, 3.0, 1, {255,20,10,240});
		TE_SendToAll();
	}
}

public Action Timer_DefuseHegrenade(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
		SetEntProp(ent, Prop_Data, "m_nNextThinkTick", -1);
}

public Action Timer_DetonateHegrenade(Handle timer, any ref)
{
	int ent = EntRefToEntIndex( ref );
	if ( ent != INVALID_ENT_REFERENCE )
	{
		HeGrenadeExplode(ent);
		
		SetEntProp(ent, Prop_Data, "m_nNextThinkTick", 1);
		SetEntProp(ent, Prop_Data, "m_takedamage", 2 );
		SetEntProp(ent, Prop_Data, "m_iHealth", 1 );
		SDKHooks_TakeDamage(ent, 0, 0, 1.0);
	}
}

void HeGrenadeExplode(int entity)
{
	float pos[3];
	Entity_GetAbsOrigin(entity, pos);
	pos[2] += 32.0;
	
	Env_Explosion(0, -1, pos, "weapon_artilerie", g_iEventArtilerieMagnitude, 0, 0.0, 0);
}