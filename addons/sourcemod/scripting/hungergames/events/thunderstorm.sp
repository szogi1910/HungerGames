int g_iLightningsLeft;

int g_iLightlingTarget;

void StartThunderStorm(int target)
{
	g_iLightlingTarget = target;
	
	g_iLightningsLeft = g_iEventThunderStormLightnings;
	CreateTimer(g_fEventThunderStormInterval, Timer_ThunderStorm, _, TIMER_FLAG_NO_MAPCHANGE);
	SpawnFuncPrecipitation(0, g_fEventThunderStormInterval * float(g_iEventThunderStormLightnings), {75, 255, 75, 200});
}

public Action Timer_ThunderStorm(Handle timer, any data)
{
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
		
	if(g_iLightningsLeft > 0)
	{
		g_iLightningsLeft--;
		CreateTimer(g_fEventThunderStormInterval, Timer_ThunderStorm, _, TIMER_FLAG_NO_MAPCHANGE);
		ThunderStorm();
	}
	
	return Plugin_Stop;
}

void ThunderStorm()
{
	if (GetRandomInt(0, 100) > 70.0)
	{
		float distance;
		GetRoofDistance(g_iLightlingTarget, distance);
			
		float clientpos[3]; 
		GetClientAbsOrigin(g_iLightlingTarget, clientpos);
		//clientpos[2] += 750.0;
		
		float weatherpos[3]; 
		weatherpos[0] = clientpos[0]+GetRandomInt(-150, 150);
		weatherpos[1] = clientpos[1]+GetRandomInt(-150, 150);
		weatherpos[2] = clientpos[2]+GetRandomInt(-50, 50)+2000.0;
		
		// Player is safe
		if(distance < 256.0)
		{
			clientpos[0] += GetRandomInt(-2500, 2500);
			clientpos[1] += GetRandomInt(-2500, 2500);
			clientpos[2] += GetRandomInt(0, 2000);
		}
		else Point_Hurt(g_iLightlingTarget, g_iEventThunderStormDamage);
			
		TE_SetupBeamPoints(clientpos, weatherpos, g_iLaser, 0, 0, 0, 1.0, 1.0, 70.0, 0, 25.0, {225, 225, 255, 255}, 3);
		TE_SendToAll();
		
		EmitAmbientSound(sndThunderStorm[GetRandomInt(0, sizeof(sndThunderStorm)-1)], weatherpos, g_iLightlingTarget, SNDLEVEL_RAIDSIREN);
	}
}