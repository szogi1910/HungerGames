Handle g_hIonCannonTimer = null;

float g_fIonCenter[3];
float g_fIonStepSize;

float g_fIonDistance;
float g_fIonNphi;

int g_iIonTarget;

// Start Ion-Cannon satellite
void StartIonCannon(int target)
{
	g_iIonTarget = target;
	
	if (g_hIonCannonTimer != null)
	{
		CloseHandle(g_hIonCannonTimer);
		g_hIonCannonTimer = null;
	}
	
	if(g_bSndIonApproaching)
		EmitSoundToAllAny(sndIonApproaching);
	
	g_hIonCannonTimer = CreateTimer(8.0, Timer_IonApproaching);
}

// Lock target
public Action Timer_IonApproaching(Handle timer, any data)
{
	g_hIonCannonTimer = null;
	
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
		
	if(!IonCannon_LockTarget())
	{
		// Loop until we have a target
		g_hIonCannonTimer = CreateTimer(1.0, Timer_IonApproaching);
		return Plugin_Stop;
	}
	
	// We got a target, announce it before we start the attack
	
	if(g_bSndIonDeployed)
		EmitSoundToAllAny(sndIonDeployed);
		
	g_hIonCannonTimer = CreateTimer(3.5, Timer_IonDeployed);
	
	return Plugin_Stop;
}

bool IonCannon_LockTarget()
{
	GetClientAbsOrigin(g_iIonTarget, g_fIonCenter);
	
	g_fIonDistance = g_fEventIonCannonStartRange;
	g_fIonNphi = 0.0;
	
	g_fIonStepSize = g_fIonDistance / (13.0 / g_fEventIonCannonUpdateInterval) * 2.0;
	
	return true;
}

// Start charging the weapon
public Action Timer_IonDeployed(Handle timer, any data)
{
	g_hIonCannonTimer = null;
	
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
	
	if(g_bSndIonCharging)
		EmitSoundToAllAny(sndIonCharging, 0, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, g_fIonCenter);
	
	IonAttack();
	
	return Plugin_Stop;
}

public Action DrawIon(Handle hTimer, any data)
{
	g_hIonCannonTimer = null;
	
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
		return Plugin_Stop;
		
	IonAttack();
	
	return Plugin_Stop;
}

public void IonAttack()
{
	float position[3];
	
	if (g_fIonDistance > 0)
	{
		// Stage 1
		float s = Sine(g_fIonNphi/360*6.28)*g_fIonDistance;
		float c = Cosine(g_fIonNphi/360*6.28)*g_fIonDistance;
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[2] = g_fIonCenter[2];
		
		position[0] += s;
		position[1] += c;
		DrawIonBeam(position);

		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] -= s;
		position[1] -= c;
		DrawIonBeam(position);
		
		// Stage 2
		s=Sine((g_fIonNphi+45.0)/360*6.28)*g_fIonDistance;
		c=Cosine((g_fIonNphi+45.0)/360*6.28)*g_fIonDistance;
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] += s;
		position[1] += c;
		DrawIonBeam(position);
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] -= s;
		position[1] -= c;
		DrawIonBeam(position);
		
		// Stage 3
		s=Sine((g_fIonNphi+90.0)/360*6.28)*g_fIonDistance;
		c=Cosine((g_fIonNphi+90.0)/360*6.28)*g_fIonDistance;
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] += s;
		position[1] += c;
		DrawIonBeam(position);
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] -= s;
		position[1] -= c;
		DrawIonBeam(position);
		
		// Stage 4
		s=Sine((g_fIonNphi+135.0)/360*6.28)*g_fIonDistance;
		c=Cosine((g_fIonNphi+135.0)/360*6.28)*g_fIonDistance;
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] += s;
		position[1] += c;
		DrawIonBeam(position);
		
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[0] -= s;
		position[1] -= c;
		DrawIonBeam(position);

		if (g_fIonNphi >= 360)
			g_fIonNphi = 0.0;
		else
			g_fIonNphi += 5.0; // Rotation speed
		
		float fMaxDistance = 2.1 * g_fEventIonCannonStartRange;
		float fMaxPull = -150.0;
		
		LoopTributes(client)
		{
			float fClientPos[3];
			GetClientAbsOrigin(client, fClientPos);
			
			float fDistance = GetVectorDistance(g_fIonCenter, fClientPos);
			if(fDistance > fMaxDistance)
				continue;
			
			float fVelocity[3];
			MakeVectorFromPoints(g_fIonCenter, fClientPos, fVelocity);
			
			NormalizeVector(fVelocity, fVelocity);
			ScaleVector(fVelocity, fMaxPull);
			
			float fVel[3];
			Entity_GetAbsVelocity(client, fVel);
			AddVectors(fVelocity, fVel, fVelocity);
			
			TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVelocity);
		}
	}
	
	g_fIonDistance -= g_fIonStepSize; // shrink rate
	
	if (g_fIonDistance > 50.0)
	{
		g_hIonCannonTimer = CreateTimer(g_fEventIonCannonUpdateInterval, DrawIon);
	}
	else
	{
		position[0] = g_fIonCenter[0];
		position[1] = g_fIonCenter[1];
		position[2] += 1500.0;
		
		position[2] = g_fIonCenter[2] + 50.0;
		
		Env_Shake(g_fIonCenter, 120.0, 2000.0, 15.0, 250.0);
		
		//TE_SetupExplosion(g_fIonCenter, g_iExplosive, 10.0, 1, 0, 0, 5000);
		//TE_SendToAll();
		
		TE_SetupBeamRingPoint(position, 0.0, 1500.0, g_iBlueGlow, g_iHalo, 0, 0, 0.5, 100.0, 5.0, {150, 255, 255, 255}, 0, 0);
		TE_SendToAll();
		TE_SetupBeamRingPoint(position, 0.0, 1500.0, g_iBlueGlow, g_iHalo, 0, 0, 5.0, 100.0, 5.0, {255, 255, 255, 255}, 0, 0);
		TE_SendToAll();
		TE_SetupBeamRingPoint(position, 0.0, 1500.0, g_iBlueGlow, g_iHalo, 0, 0, 2.5, 100.0, 5.0, {255, 255, 255, 255}, 0, 0);
		TE_SendToAll();
		TE_SetupBeamRingPoint(position, 0.0, 1500.0, g_iBlueGlow, g_iHalo, 0, 0, 6.0, 100.0, 5.0, {255, 255, 255, 255}, 0, 0);
		TE_SendToAll();
		
		// Light
		int ent = CreateEntityByName("light_dynamic");
		
		DispatchKeyValue(ent, "_light", "255 255 255 255");
		DispatchKeyValue(ent, "brightness", "5");
		DispatchKeyValueFloat(ent, "spotlight_radius", 500.0);
		DispatchKeyValueFloat(ent, "distance", 1500.0);
		DispatchKeyValue(ent, "style", "6");
		
		DispatchSpawn(ent);
		AcceptEntityInput(ent, "TurnOn");
		
		TeleportEntity(ent, position, NULL_VECTOR, NULL_VECTOR);
		
		RemoveEntity(ent, 3.0);
		
		// Sound
		
		if(g_bSndIonImplosion)
			EmitSoundToAllAny(sndIonImplosion, 0, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL, -1, g_fIonCenter);
		
		float fMaxDistance = 2.5 * g_fEventIonCannonStartRange;
		float fMaxDamage = 5000.0;
		
		LoopTributes(client)
		{
			float fClientPos[3];
			GetClientAbsOrigin(client, fClientPos);
			
			float fDistance = GetVectorDistance(g_fIonCenter, fClientPos);
			if(fDistance > fMaxDistance)
				continue;
				
			int iDamage = RoundToFloor(fMaxDamage * (fMaxDistance / fDistance / 100.0));
				
			Point_Hurt(client, iDamage);
		}
	}
}

void DrawIonBeam(float fStartPos[3])
{
	float position[3];
	position[0] = fStartPos[0];
	position[1] = fStartPos[1];
	position[2] = fStartPos[2] + 1500.0;	

	TE_SetupBeamPoints(fStartPos, position, g_iLaser, 0, 0, 0, g_fEventIonCannonUpdateInterval, 25.0, 25.0, 0, 1.0, {0, 150, 255, 255}, 3 );
	TE_SendToAll();
	
	position[2] -= 1490.0;
	
	TE_SetupSmoke(fStartPos, g_iFireSmoke, 10.0, 2);
	TE_SendToAll();
	
	TE_SetupGlowSprite(fStartPos, g_iBlueGlow, 1.0, 1.0, 255);
	TE_SendToAll();
}