bool g_bEventOn;

float g_fOrigin[3];
float g_fDir[3];

void ResetFireworks()
{
	g_bEventOn = false;
}

public Action Timer_StopFireworks(Handle timer, any data)
{
	g_bEventOn = false;
	
	return Plugin_Stop;
}

void StartFireworks(int winner)
{
	if(!g_bFireworksEnabled)
		return;
	
	g_bEventOn = true;
	Fireworks(winner);
	
	CreateTimer(2.0, Fireworks_Timer, GetClientUserId(winner), TIMER_REPEAT);
}

public Action Fireworks_Timer(Handle timer, any userid)
{
	if(g_bEventOn)
	{
		Fireworks(userid);
		return Plugin_Continue;
	}
	
	return Plugin_Stop;
}

void Fireworks(int userid)
{
	int client = GetClientOfUserId(userid);
	if(!client || !IsClientInGame(client) || !IsPlayerAlive(client))
		return;
	
	Handle aSounds = CreateArray(1);
	
	if(g_bSndFireworks1)
		PushArrayCell(aSounds, 1);
	if(g_bSndFireworks2)
		PushArrayCell(aSounds, 2);
	if(g_bSndFireworks3)
		PushArrayCell(aSounds, 3);
	if(g_bSndFireworks4)
		PushArrayCell(aSounds, 4);
	
	int iSounds = GetArraySize(aSounds);
	if(iSounds != 0)
	{
		int random = GetArrayCell(aSounds, GetRandomInt(0, iSounds-1));
		
		if(random == 1)
			EmitSoundToAllAny(sndFireworks1, _, _, SNDLEVEL_DRYER, _, SNDVOL_NORMAL, _, _, _, _, _, _); 
		else if(random == 2)
			EmitSoundToAllAny(sndFireworks2, _, _, SNDLEVEL_DRYER, _, SNDVOL_NORMAL, _, _, _, _, _, _); 
		else if(random == 3)
			EmitSoundToAllAny(sndFireworks3, _, _, SNDLEVEL_DRYER, _, SNDVOL_NORMAL, _, _, _, _, _, _); 
		else if(random == 4)
			EmitSoundToAllAny(sndFireworks4, _, _, SNDLEVEL_DRYER, _, SNDVOL_NORMAL, _, _, _, _, _, _);
	}
	
	CloseHandle(aSounds);
	
	GetClientAbsOrigin(client, g_fOrigin);
	
	float FireworkHeight;
	FireworkHeight = GetRandomFloat(400.0, 600.0);
	g_fOrigin[2] = (g_fOrigin[2] + FireworkHeight);
	
	int FireworkType = GetRandomInt(1, 3);
	
	if(FireworkType == 1)
	{
		TE_SetupSparks(g_fOrigin, g_fDir, 1000, 1000);
		TE_SendToAll();
	}
	else if(FireworkType == 2)
	{
		float g_fAir[3];
		
		TE_SetupSparks(g_fOrigin, g_fDir, 1000, 1000);
		TE_SendToAll();
		
		AddVectors(g_fOrigin, g_fAir, g_fAir);
		g_fAir[2] = (g_fAir[2] - 50.0);
		g_fAir[1] = (g_fAir[1] + 100.0);
		
		TE_SetupSparks(g_fAir, g_fDir, 1000, 1000);
		TE_SendToAll();
	}
	else
	{
		float g_fAir[3];
		
		TE_SetupSparks(g_fOrigin, g_fDir, 1000, 1000);
		TE_SendToAll();
		
		AddVectors(g_fOrigin, g_fAir, g_fAir);
		g_fAir[2] = (g_fAir[2] - 50.0);
		g_fAir[1] = (g_fAir[1] + 100.0);
		
		TE_SetupSparks(g_fOrigin, g_fDir, 1000, 1000);
		TE_SendToAll();

		g_fAir[2] = (g_fAir[2] + 25.0);
		g_fAir[0] = (g_fAir[0] + 100.0);
		
		TE_SetupSparks(g_fOrigin, g_fDir, 1000, 1000);
		TE_SendToAll();
	}
}