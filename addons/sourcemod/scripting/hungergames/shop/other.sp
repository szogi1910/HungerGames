void Menu_ShopOther(int client)
{
	if(!CanUseShop(client, false, true))
		return;
	
	Menu menu = new Menu(MenuHandler_ShopOther);
	
	menu.SetTitle("%T", "Shop_OtherTitle", client, Stats_GetPoints(client));
	
	int iPrice;
	char sItem[32];
	
	/* Binoculars */
	
	iPrice = g_iShopBinocularsPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "Binoculars", client);
		menu.AddItem("binoculars", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	/* Parachute */
	
	iPrice = g_iShopGpsPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "Parachute", client);
		menu.AddItem("parachute", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	/* Compass */
	
	iPrice = g_iShopCompassPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "Compass", client);
		menu.AddItem("compass", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	/* Tracer */
	
	iPrice = g_iShopTracerPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "Tracer", client);
		menu.AddItem("tracer", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	/* Jammer */
	
	iPrice = g_iShopJammerPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "Jammer", client);
		menu.AddItem("jammer", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	/* GPS */
	
	iPrice = g_iShopGpsPrice;
	
	if(iPrice > 0)
	{
		Format(sItem, sizeof(sItem), "%T", "GPS", client);
		menu.AddItem("gps", sItem, Stats_GetPoints(client) < iPrice ? ITEMDRAW_DISABLED : ITEMDRAW_DEFAULT);
	}
	
	char sBuffer[64];
	Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Back", client);
	menu.AddItem("back", sBuffer, ITEMDRAW_DEFAULT);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_ShopOther(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "back"))
		{
			Menu_Shop(client);
			return;
		}
		
		if(!CanUseShop(client, false, true))
			return;
		
		int iPrice;
		
		if(StrEqual(sInfo, "binoculars"))
			iPrice = g_iShopBinocularsPrice;
		else if(StrEqual(sInfo, "compass"))
			iPrice = g_iShopCompassPrice;
		else if(StrEqual(sInfo, "tracer"))
			iPrice = g_iShopTracerPrice;
		else if(StrEqual(sInfo, "jammer"))
			iPrice = g_iShopJammerPrice;
		else if(StrEqual(sInfo, "gps"))
			iPrice = g_iShopGpsPrice;
		else if(StrEqual(sInfo, "parachute"))
			iPrice = g_iShopParachutePrice;
		
		if(!IsValidShopTarget(g_iShopTarget[client]))
		{
			CPrintToChat(client, "%T", "Shop_InvalidTarget", client, g_sPrefix);
			return;
		}
		
		if(!Stats_RemovePoints(client, iPrice))
		{
			CPrintToChat(client, "%T", "Shop_NotEnough", client, g_sPrefix, iPrice, Stats_GetPoints(client));
			return;
		}
		
		g_iShopLastTarget[client] = g_iShopTarget[client];
		
		if(StrEqual(sInfo, "binoculars"))
			GiveBinoculars(g_iShopTarget[client]);
		else if(StrEqual(sInfo, "compass"))
			GiveCompass(g_iShopTarget[client]);
		else if(StrEqual(sInfo, "tracer"))
			GiveTracer(g_iShopTarget[client]);
		else if(StrEqual(sInfo, "jammer"))
			GiveJammer(g_iShopTarget[client]);
		else if(StrEqual(sInfo, "gps"))
			GiveGPS(g_iShopTarget[client]);
		else if(StrEqual(sInfo, "parachute"))
			GiveParachute(g_iShopTarget[client]);
	}
	else if (action == MenuAction_End)
		delete menu;
}