void StartPlaytimeTimer()
{
	CreateTimer(60.0, Timer_Playtime, _, TIMER_REPEAT);
}

public Action Timer_Playtime(Handle timer, any data)
{
	LoopIngameClients(client)
	{
		if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
			continue;
		
		g_iPlaytime[client]++;
		g_iPlaytimeTemp[client]++;
		
		int iPoints = g_iPointsPlaytime;
		
		if(Client_HasAdminFlags(client, g_iVIPflags))
			iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
		
		Stats_AddPoints(client, iPoints, false);
		
		if (g_iPlaytimeTemp[client] > 5)
			Stats_UpdatePlaytime(client);
	}
	
	return Plugin_Continue;
}