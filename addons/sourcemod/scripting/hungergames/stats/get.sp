//

stock int Stats_GetPoints(int client)
{
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
		return -1;
	
	return g_iPoints[client];
}